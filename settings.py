#This file has all setting related stuff: keybinds, display size, saving and loading

import json, os, pygame, ctypes
from pygame.locals import *


# This line is stop windows from scaling the window. FIXME: Check if linux supports this
ctypes.windll.user32.SetProcessDPIAware()

displayModes = {"FULLSCREEN": pygame.FULLSCREEN,
                "BORDERLESS": pygame.NOFRAME, "WINDOWED": 0, "SCALED": pygame.SCALED}


standardSettings = {"resolution": (0, 0),
                    "displayMode": "SCALED",
                    "firstLaunch": False,
                    "KEYTOBINDING": {K_e: "SELECT", K_r: "ROTATE"},
                    "MOUSEBINDING": ["LEFT", "MIDDLE", "RIGHT"]
                    }

'''
Settings is a wrapper for all keybinding and input related stuff
It checks if all keys are there and checks if the monitor supports the resolution
'''

class Settings():
    def __init__(self):
        self.settings = standardSettings
        if os.path.isfile("settings.json"):
            with open("settings.json", 'r') as f:
                self.settings = json.load(f)
                complete = True
                for key in standardSettings:
                    if not(key in self.settings):
                        complete = False
                if not(complete):
                    print("Settings are invalid")
                    self.settings = standardSettings

        print("Loaded settings: ", self.settings)
        self.initGraphics()

    def initGraphics(self):
        pygame.display.set_caption("Battleships")
        availableModes = pygame.display.list_modes()
        self.settings["resolution"] = (
            self.settings["resolution"][0], self.settings["resolution"][1])
        if (self.settings["firstLaunch"]):
            self.screen = pygame.display.set_mode(
                availableModes[0], displayModes["SCALED"])
        else:
            if self.settings["resolution"] in availableModes and self.settings["displayMode"] in displayModes:
                print("Found resolution in monitor: ",
                      self.settings["resolution"])
                self.screen = pygame.display.set_mode(
                    self.settings["resolution"], displayModes[self.settings["displayMode"]])
            else:
                print("Defaulting to best monitor resolution: ",
                      availableModes[0])
                self.screen = pygame.display.set_mode(
                    availableModes[0], displayModes["SCALED"])
                self.settings["resolution"] = availableModes[0]
                self.settings["displayMode"] = "SCALED"


    #TODO: Add changing of settings

    def getScreen(self):
        return self.screen

    def save(self):
        tosave = self.settings
        tosave["firstLaunch"] = False
        print("Saving: ", tosave)
        with open("settings.json", "w") as f:
            json.dump(tosave, f)

    def reset(self):
        self.settings = standardSettings
