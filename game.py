from gamestates import HandleGameStates
import pygame

class Game():
    def __init__(self, allDraw, width):
        self.turn = True #True is player 1, false is player2
        self.player1 = HandleGameStates(allDraw, width,0) #Player 1
        self.player2 = HandleGameStates(allDraw, width,1) #Player 2

    def changeView(self):
        self.turn = not(self.turn) 
        print("Turn is for: ", "player1" if self.turn else "player2")

    def draw(self):
        if self.turn:
            self.player1.draw()
        else:
            self.player2.draw()

    def update(self, mouse,key):
        self.player1.update(mouse,key,self.turn)
        self.player2.update(mouse,key,not(self.turn))
        self.checkShootReady()

    def checkShootReady(self):
        if self.player1.gameState == 3 and self.player2.gameState == 3:
            player1GameState = self.player1.gameStates[self.player1.gameState]
            player2GameState = self.player2.gameStates[self.player2.gameState]
            if (player1GameState.ready and player2GameState.ready):
                target11 = [player1GameState.targeted.rect.x,player1GameState.targeted.rect.y]
                target22 = [player2GameState.targeted.rect.x,player2GameState.targeted.rect.y]
                if target11[0] > 0 and target22[0] > 0:

                    #Player 1 hit detection
                    target1 = self.player1.minusGrid(target11)
                    player1GameState.targeted.move((target1[0], target1[1]))

                    #Target1 is on own grid and target11 is on the unkown grid
 
                    player1Hit = pygame.sprite.spritecollide(player1GameState.targeted,player2GameState.shipSprites,False)
                    if len(player1Hit) > 0:
                        print("PLAYER 1 HIT")
                        player2GameState.hit(target1)
                        player1GameState.hitSelf(target11)
                    else:
                        player2GameState.miss(target1)
                        player1GameState.missSelf(target11)


                    #Player 2 hit detection
                    target2 = self.player2.minusGrid(target22)
                    player2GameState.targeted.move((target2[0], target2[1]))

                    #Target2 is on own grid and target22 is on the unkown grid

                    player2Hit = pygame.sprite.spritecollide(player2GameState.targeted,player1GameState.shipSprites,False)
                    if len(player2Hit) > 0:
                        print("PLAYER 2 HIT")
                        player1GameState.hit(target2)
                        player2GameState.hitSelf(target22)
                    else:
                        player1GameState.miss(target2)
                        player2GameState.missSelf(target22)

                    #TODO: Show popup with result

                    #Moving away target locators
                    player1GameState.targeted.move((-100,-100))
                    player2GameState.targeted.move((-100,-100))


