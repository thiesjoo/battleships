# Has all drawing functions, including loading of sprites, showcasing ships and 

import pygame, json, copy

class drawFunctions():
    def __init__(self, screen):
        self.screen = screen

    #Draw gets an array of sprites and just draws them to the screen
    def draw(self,toDraw):
        for i in range(0,len(toDraw)):
            toDraw[i].draw(self.screen)



'''
imageSprite is a class to load an image, format it to the blocksize and make some functions to rotate and move the image
'''

class imageSprite(pygame.sprite.Sprite):
    def __init__(self,block_size, width,height,location):
        super().__init__()
        self.image = pygame.image.load(location).convert_alpha()
        self.image = pygame.transform.scale(
                self.image, (block_size*width-4, block_size*height-4))

        self.rect = self.image.get_rect()
                
    def rotate(self, angle):
        self.image = pygame.transform.rotate(self.image, angle)
        tempx = self.rect.x
        tempy = self.rect.y
        self.rect = self.image.get_rect()
        self.move((tempx,tempy))

    def move(self, location):
        self.rect.x = location[0] + 2
        self.rect.y = location[1] + 2

    def get_width(self):
        return self.image.get_width()

    def get_height(self):
        return self.image.get_height()