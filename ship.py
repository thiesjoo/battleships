# This file houses the ship and selectship logic.

import pygame, json, copy
from draw import imageSprite
RED = (255, 0, 0)
shipTypes = []

with open("assets/ships.json", 'r') as f:
    data = json.load(f)
    shipTypes = data
    ships = list(data.keys())

class Ship(imageSprite):
    def __init__(self, shipType, block_size):
        self.blockSize = block_size
        self.shipType = shipType
        if shipType in shipTypes:
            self.length = shipTypes[shipType]
            super().__init__(block_size,1,self.length,f"assets/{shipType}/Hull.png")
        else:
            print("SHIP NOT FOUND")
            self.length = 0
            self.image = pygame.Surface((block_size-3, block_size*1-3))
            self.image.fill((255, 52, 42))
        self.rect = self.image.get_rect()

GOOD = (0, 60, 60)
BAD = (255, 60, 60)


'''
SelectShip handles all logic for the 'create' part of the game
It checks for collisions and out of bound ships.
It also highlights the places the ship will go to.

'''

class SelectShip():
    def __init__(self, margin, blockSize, grid1, mouseToCoords):
        self.selectedShip = ""
        self.resetShip = Ship("", blockSize)
        self.ship = self.resetShip
        self.grid1 = grid1
        self.blockSize = blockSize
        self.mouseToCoords = mouseToCoords
        self.rotation = 0
        self.TOTAL_MARGIN = margin
        self.highLightColor = BAD
        self.coords = (0,0,0)
        self.shipTypes = shipTypes
        self.ships = copy.deepcopy(ships)
        self.currentSelect = pygame.sprite.GroupSingle() 
        self.placed = pygame.sprite.Group()
        self.switchShip()
        self.status = False

    def logic(self, x, y):
        if not(self.selectedShip == "STOP"):
            coords = self.mouseToCoords(x, y)
            self.coords = coords
            if coords[2] == 1:
                self.grid1.disableHighLight()
                self.highLightColor = GOOD
                # X = (mouse coords - shiplength(Offset for rotation Only with 270 and 180) + 1(Offset for rotation Only with 270 and 180))* blocksize(Size of each block) + margin(MArgin from side of screen to grid)
                # Y = (mouse coords * blocksize + margin)
                for i in range(0, self.ship.length):
                    if self.rotation == 0:
                        if ((coords[1]+i) > -1 and coords[1]+i < 10 and coords[0] > -1 and coords[0] < 10):
                            self.ship.move(
                                [coords[0]*self.blockSize+self.TOTAL_MARGIN, (coords[1])*self.blockSize+self.TOTAL_MARGIN])
                            self.grid1.highLight(
                                coords[0], coords[1]+i, coords[2], self.highLightColor)
                        else:
                            self.highLightColor = BAD
                            self.grid1.highLight(
                                coords[0], coords[1]+i, coords[2], self.highLightColor)
                    elif self.rotation == 90:
                        if (coords[0]+i > -1 and coords[0]+i < 10 and coords[1] > -1 and coords[1] < 10):
                            self.ship.move(
                                [coords[0]*self.blockSize+self.TOTAL_MARGIN, (coords[1])*self.blockSize+self.TOTAL_MARGIN])
                            self.grid1.highLight(
                                coords[0]+i, coords[1], coords[2], self.highLightColor)
                        else:
                            self.highLightColor = BAD
                            self.grid1.highLight(
                                coords[0]+i, coords[1], coords[2], self.highLightColor)

                    elif self.rotation == 180:
                        if (coords[1]-i > -1 and coords[1]-i < 10 and coords[0] > -1 and coords[0] < 10):
                            self.ship.move([coords[0]*self.blockSize+self.TOTAL_MARGIN,
                                            (coords[1]-self.ship.length+1)*self.blockSize+self.TOTAL_MARGIN])
                            self.grid1.highLight(
                                coords[0], coords[1]-i, coords[2], self.highLightColor)
                        else:
                            self.highLightColor = BAD
                            self.grid1.highLight(
                                coords[0], coords[1]-i, coords[2], self.highLightColor)
                    elif self.rotation == 270:
                        if (coords[0]-i > -1 and coords[0]-i < 10 and coords[1] > -1 and coords[1] < 10):
                            self.ship.move([(coords[0]-self.ship.length+1)*self.blockSize +
                                            self.TOTAL_MARGIN, (coords[1])*self.blockSize+self.TOTAL_MARGIN])
                            self.grid1.highLight(
                                coords[0]-i, coords[1], coords[2], self.highLightColor)
                        else:
                            self.highLightColor = BAD
                            self.grid1.highLight(
                                coords[0]-i, coords[1], coords[2], self.highLightColor)

                    collision = pygame.sprite.spritecollide(self.ship, self.placed, False)
                    if collision:
                        self.highLightColor = BAD    
            else:
                self.highLightColor = BAD
            if (self.highLightColor == BAD):
                self.status = False
            else:
                self.status = True
            self.grid1.changeColor(self.highLightColor)
        elif self.selectedShip == "STOP":
            self.grid1.disableHighLight()
            return True
            # If ships are empty return True so game continues
        return False

    def rotate(self):
        self.rotation = (self.rotation + 90) % 360
        self.ship.rotate(90)
        print("Rotation now is ", self.rotation)

    def remove(self):
        index = 0
        try:
            if (self.status): #Same as highlight color, but boolean
                index = self.ships.index(self.selectedShip) # If it is not found it throws ValueError
                toReturn = {"name": self.selectedShip, "coords": self.coords, "rotation": self.rotation}
                self.placed.add(self.ship)
                self.ships.pop(index)
                self.switchShip(index)
                return toReturn
            else:
                print("INVALID LOCATION")
                return False
        except ValueError as e:
            print("Cant remove more ships")
            return False

    def switchShip(self, index=-1):
        if (len(self.ships) == 0):
            print("No ships left")
            self.currentSelect.empty()
            self.selectedShip = "STOP"
        elif (len(self.ships) >= -1):
            if index == -1:
                try:
                    index = self.ships.index(self.selectedShip)
                except ValueError as e:
                    print("Starting up: ValueError in ship.py")
            self.selectedShip = self.ships[(index + 1) % len(self.ships)]
            self.rotation = 0
            self.currentSelect.empty()
            self.ship = Ship(self.selectedShip, self.blockSize)
            self.ship.move((-100,-100))
            self.currentSelect.add(self.ship)
        else:
            print("Nope, Something went wrong in switchShip(ship.py)")
            self.selectedShip = "STOP"


'''
ShowCaseShips is a class which shows all ships you have left to choose from
'''

class ShowCaseShips(pygame.sprite.Sprite):
    def __init__(self,block_size,margin,color, ships):
            pygame.sprite.Sprite.__init__(self)
            self.blockSize = block_size
            self.margin = margin
            self.ships = copy.deepcopy(ships)
            self.shipSprites = pygame.sprite.Group()
            #5 because largest ship is 5 blocks 
            self.image = pygame.Surface([self.blockSize*(5), self.blockSize*len(ships)],pygame.SRCALPHA, 32)
            self.font = pygame.font.SysFont('arial', 30, False)
            self.rect = self.image.get_rect()

    def removeShip(self, name):
        self.ships.pop(name)
        self.move([self.rect.x,self.rect.y]) #Updates sprite group

    def move(self, location):
        self.rect.x = location[0]
        self.rect.y = location[1]
        tempCount = 0
        self.shipSprites.empty()
        for ship in self.ships:
            toAdd = Ship(ship, self.blockSize)
            toAdd.move([self.rect.x, tempCount*self.blockSize+20])
            toAdd.rotate(90)
            self.shipSprites.add(toAdd)
            tempCount += 1