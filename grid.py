import pygame, string

'''
Grid draws a grid(10x10) on screen with lines. This is for performance reasons(Drawing rectancles takes a lot of GPU power)
It also houses some highlight logic so you don't have to manage all highlighted squares

'''

class Grid(pygame.sprite.Sprite):
    def __init__(self, screenSize, blockSize, marginSize, lineColor):
        # Call the parent class (Sprite) constructor
        super().__init__()

        # Cosmetics
        self.LINE_COLOR = lineColor
        self.axisLabels = True
        self.font = pygame.font.SysFont('arial', 30, False)

        self.GRID_COORD_MARGIN_SIZE = marginSize
        self.numberOfBlocks = 10
        self.blockSize = blockSize
        self.screenSize = screenSize
        self.image = pygame.Surface(
            [screenSize+self.GRID_COORD_MARGIN_SIZE*2, screenSize+self.GRID_COORD_MARGIN_SIZE*2],pygame.SRCALPHA, 32)

        self.playerSprites = pygame.sprite.Group()
        self.tempSprites = pygame.sprite.Group()

        #Rows
        for rows in range(self.numberOfBlocks):
            rowCoord = self.GRID_COORD_MARGIN_SIZE + rows * blockSize
            if self.axisLabels:
                if rows+1 < 10:
                    ident = '    '
                else:
                    ident = '   '
                text = self.font.render(ident + str(rows+1), 1, (0, 0, 0))
                # TODO: Draw on other image and draw that image on top of everything
                self.image.blit(text, (-10, rowCoord+(blockSize/2)))
            pygame.draw.line(self.image, self.LINE_COLOR,
                             (self.GRID_COORD_MARGIN_SIZE, rowCoord), (screenSize+self.GRID_COORD_MARGIN_SIZE, rowCoord))

        # Columns
        for co in range(self.numberOfBlocks):
            colCoord = self.GRID_COORD_MARGIN_SIZE + co * blockSize
            if self.axisLabels:
                if co < 10:
                    ident = '  '
                else:
                    ident = ' '
                text = self.font.render(
                    ident + str(string.ascii_uppercase[co]), 1, (0, 0, 0))
                    #string.ascii_uperrcase is to get the uppercase value with a alphabet index
                self.image.blit(text, (colCoord+blockSize/2, 10))

            pygame.draw.line(self.image, self.LINE_COLOR, (colCoord,
                                                           self.GRID_COORD_MARGIN_SIZE), (colCoord, screenSize+self.GRID_COORD_MARGIN_SIZE))

        # End lines
        # Right side line
        pygame.draw.line(self.image, self.LINE_COLOR, (screenSize+self.GRID_COORD_MARGIN_SIZE,
                                                       self.GRID_COORD_MARGIN_SIZE), (screenSize+self.GRID_COORD_MARGIN_SIZE, screenSize+self.GRID_COORD_MARGIN_SIZE))
        # Underline                                               
        pygame.draw.line(self.image, self.LINE_COLOR,
                         (self.GRID_COORD_MARGIN_SIZE, screenSize+self.GRID_COORD_MARGIN_SIZE), (screenSize+self.GRID_COORD_MARGIN_SIZE, screenSize+self.GRID_COORD_MARGIN_SIZE))


        self.rect = self.image.get_rect()


    def addDraw(self,todraw,x,y):
        todraw.move([self.rect.x+self.GRID_COORD_MARGIN_SIZE+x*self.blockSize,self.rect.y+ self.GRID_COORD_MARGIN_SIZE+y*self.blockSize])
        self.playerSprites.add(todraw)

    def wipeDraw(self):
        self.playerSprites.empty()

    def changeColor(self,color):
        self.tempSprites.update(color)

    def highLight(self,x,y,grid,color):
        self.tempSprites.add(HighLight(self.blockSize, x, y,grid, self.GRID_COORD_MARGIN_SIZE,self.screenSize,color))

    def disableHighLight(self):
        self.tempSprites.empty()

    def move(self, location):
        self.rect.x = location[0]
        self.rect.y = location[1]


'''
Highlight is just a class which select 1 tile in the game and gives it a different color
'''


class HighLight(pygame.sprite.Sprite):
    def __init__(self,block_size, x, y, grid, marginSize, gridSize, color):
        pygame.sprite.Sprite.__init__(self)
        self.blockSize = block_size
        self.GRID_COORD_MARGIN_SIZE = marginSize
        self.gridSize = gridSize
        self.image = pygame.Surface([self.blockSize, self.blockSize])
        self.image.fill(color)

        self.rect = self.image.get_rect()
        self.move(x,y,grid)

    def move(self,x,y,grid):
        self.rect.y = 20+self.GRID_COORD_MARGIN_SIZE + y * self.blockSize
        if grid == 1:
            self.rect.x = 20+self.GRID_COORD_MARGIN_SIZE + x * self.blockSize
        elif grid == 2:
            self.rect.x = 20+self.GRID_COORD_MARGIN_SIZE*3 + x * self.blockSize +self.gridSize

    def update(self,color):
        self.image.fill(color)


