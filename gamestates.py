# This file houses everything with gamestates. 1 instance should be created per player

import json,pygame

from ship import SelectShip, Ship,ShowCaseShips
from grid import Grid
from draw import imageSprite

GAMESTATES = ["menu", "pause", "create", "game", "credits"]
widthOfGrid = 0
widthOfItem = 0
MARGIN = 20
GRID_MARGIN = 10

#Colors
RED = (255, 0, 0)


'''
Each gameState has the following functions:

reset(allDraw), This resets entire class and creates new classes of everything
toDraw(), returns an array of all sprites/spritegroups that have to be drawn
update(mouseData,keyData,turn), runs every frame and can calculate stuff like ship placing,highlighting and more
setExtraInfo(info), runs on gamestate change to pass extra info like ships
getExtraInfo(), returns the extra info to pass to the next state

HandleGameStates is just a wrapper for all the different gamestates and all the helper functions
'''

class HandleGameStates():
    def __init__(self, allDraw, width,id):
        self.id = id
        global widthOfGrid,widthOfItem
        self.gameState = 2
        self.oldGameState = -1
        self.allDraw = allDraw
        widthOfGrid = int((width-MARGIN*3-GRID_MARGIN*2)/2)
        widthOfItem = widthOfGrid // 10
        self.gameStates = [None,None, CreateGameState(self.changeGameState, self.allDraw, self.mouseToCoords), GameGameState(self.changeGameState, self.allDraw,self.mouseToCoords), None, None]

    def changeGameState(self,data):
        self.gameState = self.gameState+1

    def draw(self):
        # Draw the screen. 
        # If the gamestate was updated: pass extra info to next gamestate and reset the old one so it can be reused.
        if not(self.gameState == self.oldGameState):
            if not(self.oldGameState == -1):
                if (self.oldGameState in [2]): # Only pass extra info for game
                    result = self.gameStates[self.oldGameState].getExtraInfo()
                    self.gameStates[self.gameState].setExtraInfo(result)
                    print("Passing ",result)
                self.gameStates[self.oldGameState].reset(self.allDraw)
            self.oldGameState = self.gameState
        self.allDraw.draw(self.gameStates[self.gameState].toDraw())

    def update(self,mouseData, keyData, turn):
        self.gameStates[self.gameState].update(mouseData, keyData, turn)

    def mouseToCoords(self,x,y):
        # Check if outside of screen and outside of grid
        # Also returns the grid in the third(2) location
        # Passes -1 if it is an invalid location
        if (x > widthOfGrid+MARGIN*2 and x < widthOfGrid*2+MARGIN*2 and y > MARGIN and y < MARGIN+widthOfGrid):
            return (int((x-MARGIN*2-widthOfGrid)/widthOfItem),int((y-MARGIN)/widthOfItem),2) 
        elif (x > MARGIN and x < MARGIN+widthOfGrid and y > MARGIN and y < MARGIN+widthOfGrid):
            return (int((x-MARGIN)/widthOfItem),int((y-MARGIN)/widthOfItem),1)
        else:
            return (-1,-1,-1)

    def minusGrid(self,location):
        #This function shifts the coords 1 grid, used in main game logic
        newLocation = [0]*2
        newLocation[1] = location[1]
        newLocation[0] = location[0]-widthOfGrid-MARGIN
        return newLocation


class CreateGameState():
    def __init__(self, changeGameState, allDraw, mouseToCoords):
        self.changeGameState = changeGameState
        self.shipStorage = {}
        self.mouseToCoords = mouseToCoords
        self.sprites = pygame.sprite.Group()
        self.createGrid()
        self.selectShipLogic = SelectShip(GRID_MARGIN+MARGIN,widthOfItem,self.grid,self.mouseToCoords)

    def update(self, mouseData, keyData, turn):
        if turn:
            if keyData["SELECT"]:
                self.selectShipLogic.switchShip()
                mouseData["changed"] = True
            if keyData["ROTATE"]:
                self.selectShipLogic.rotate()
                mouseData["changed"] = True

            if (mouseData["changed"]):
                # If there are no more ships to collect: Go to next gamestate
                if mouseData["LEFT"]:
                    result = self.selectShipLogic.remove()
                    if (result):
                        self.shipStorage[result["name"]] = {"coords":result["coords"], "rotation":result["rotation"]}
                        self.grid3.removeShip(result["name"]) #Removes the ship in the showcase
                    else:
                        a = 1
                        #FIXME: Show popup that it is not possible
                result = self.selectShipLogic.logic(mouseData["x"],mouseData["y"]) #Returns true if not ships are left
                if (result):
                    # FIXME: Show popup with game is starting
                    self.changeGameState(self.shipStorage)
                    print("Selecting finished, Going to gamestate game")

    def toDraw(self):
        # In order: The 2 grids, the already placed ships, the ship that is currently selected, the other ships
        return [self.sprites,self.grid3.shipSprites,self.grid.tempSprites,self.selectShipLogic.placed, self.selectShipLogic.currentSelect, ]

    def createGrid(self):
        # First calculate the position of the grid then create and move the grid
        # Grid 1, Your own grid
        locationOfGrid1 = [MARGIN,MARGIN]
        self.grid = Grid(widthOfGrid, widthOfItem,GRID_MARGIN, RED)
        self.grid.move(locationOfGrid1)

        with open("assets/ships.json", 'r') as f:
            data = json.load(f)
            locationOfGrid2 = [MARGIN*2+widthOfGrid,MARGIN]
            #Grid 3, The ships you can still select
            self.grid3 = ShowCaseShips(widthOfItem,MARGIN, RED, data)
            self.grid3.move(locationOfGrid2)
        self.sprites.add(self.grid)
        self.sprites.add(self.grid3)

    def getExtraInfo(self):
        return self.shipStorage

    def setExtraInfo(self,info):
        print("Setting extra info for create")

    def reset(self, allDraw):
        print("Resetting create")
        self.shipStorage = {}
        self.sprites.empty()
        self.createGrid()
        self.selectShipLogic = SelectShip(GRID_MARGIN+MARGIN,widthOfItem,self.grid,self.mouseToCoords)






class GameGameState():
    def __init__(self,changeGameState, allDraw, mouseToCoords):
        self.changeGameState = changeGameState
        self.shipStorage = {}
        self.sprites = pygame.sprite.Group() # Grids
        self.shipSprites = pygame.sprite.Group() # Ship sprites
        self.uiSprites = pygame.sprite.Group() # Aiming, buttons
        self.hitSprites = pygame.sprite.Group() # Sprites for ships that YOU hit
        self.hitSprites2 = pygame.sprite.Group() # Sprites for hit and miss icons

        self.mouseToCoords = mouseToCoords
        self.createGrid()
        self.aim = imageSprite(widthOfItem,1,1,"assets/icons/game/reticule.png")
        self.targeted = imageSprite(widthOfItem,1,1,"assets/icons/game/targeted.png")
        self.targeted.move((-100,-100))
        self.uiSprites.add(self.aim)
        self.uiSprites.add(self.targeted)

        self.ready = True #TODO: Add a button that confirms location

    def toDraw(self):
        return [self.sprites,self.shipSprites,self.uiSprites, self.hitSprites2,self.hitSprites]

    def createGrid(self):
        # First calculate the posistion of the grid then create and move the grid
        #Grid 1
        locationOfGrid1 = [MARGIN,MARGIN]
        self.grid1 = Grid(widthOfGrid, widthOfItem,GRID_MARGIN, RED)
        self.grid1.move(locationOfGrid1)

        # Grid2
        locationOfGrid2 = [MARGIN*2+widthOfGrid,MARGIN]
        self.grid2 = Grid(widthOfGrid, widthOfItem, GRID_MARGIN,RED)
        self.grid2.move(locationOfGrid2)

        self.sprites.add(self.grid1)
        self.sprites.add(self.grid2)
        

    def setExtraInfo(self,info):
        self.reset(info)

    def storageToSprites(self):
        for ship in self.shipStorage:
            tempShip = self.shipStorage[ship]
            newShip = Ship(ship, widthOfItem)
            newCoords = (tempShip["coords"][0]*widthOfItem+MARGIN+GRID_MARGIN, tempShip["coords"][1]*widthOfItem+MARGIN+GRID_MARGIN)
            newShip.rotate(tempShip["rotation"])
            newShip.move(newCoords)
            self.shipSprites.add(newShip)

    def reset(self,shipStorage):
        self.shipStorage = shipStorage
        self.sprites.empty()
        self.shipSprites.empty()
        self.hitSprites.empty()
        self.hitSprites2.empty()
        self.createGrid()
        self.storageToSprites()

    def hit(self,location):
        #Add to grid 1: hitmarker at location
        hitSprite = imageSprite(widthOfItem,1,1,"assets/icons/game/hit.png")
        hitSprite.move(location)
        self.hitSprites2.add(hitSprite)
        self.checkIfSunk()

    def miss(self,location):
        #Add to grid 1: miss marker at location
        print("Miss")
        missSprite = imageSprite(widthOfItem,1,1,"assets/icons/game/miss.png")
        missSprite.move(location)
        self.hitSprites2.add(missSprite)
        self.checkIfSunk()

    def hitSelf(self,location):
        #Add to grid2: hitmarker at location
        hitSprite = imageSprite(widthOfItem,1,1,"assets/icons/game/hit.png")
        print("Hit")
        hitSprite.move(location)
        self.hitSprites.add(hitSprite)

    def missSelf(self,location):
        #Add to grid2: miss marker
        print("Miss")
        missSprite = imageSprite(widthOfItem,1,1,"assets/icons/game/miss.png")
        missSprite.move(location)
        self.hitSprites2.add(missSprite)
        self.checkIfSunk()


    def checkIfSunk(self):
        print(self.hitSprites.sprites())
        sprites = self.shipSprites.sprites()
        for i in range(0,len(self.shipSprites)):
            result = pygame.sprite.spritecollide(sprites[i], self.hitSprites2, False)
            # print("Ship: ", sprites[i].shipType, " collisions: ",len(result), result)
            if (len(result) == sprites[i].length):
                print("SHIP SUNK")
                self.shipSprites.remove(sprites[i])

    def update(self, mouseData, keyData, turn):
        if (turn):
            gridCoords = self.mouseToCoords(mouseData["x"], mouseData["y"])
            x = MARGIN+GRID_MARGIN+widthOfGrid*(gridCoords[2]-1) + MARGIN*(gridCoords[2]-1) + gridCoords[0]*widthOfItem
            y = MARGIN+GRID_MARGIN+widthOfItem*gridCoords[1]
            self.aim.move((x, y))

            if (mouseData["LEFT"]):
                if (gridCoords[2] == 2):
                    oldCoordsx = self.targeted.rect.x
                    oldCoordsy = self.targeted.rect.y

                    self.targeted.move((x,y))

                    playerHit = pygame.sprite.spritecollide(self.targeted,self.hitSprites2,False)
                    playerHit2 = pygame.sprite.spritecollide(self.targeted,self.hitSprites,False)
                    print("test: ",playerHit, playerHit2)
                    if len(playerHit) > 0 or len(playerHit2) > 0:
                        print("Already shot here")
                        #TODO: Popup
                        self.targeted.move((oldCoordsx,oldCoordsy))
                else:
                    print("WTF ARE YOU EVEN TRYING")
                    #TODO: Popup
