# The main script that init's all other scripts

import pygame, time, copy, json, random

from pygame.locals import *

from draw import drawFunctions, imageSprite
from settings import Settings
from game import Game

pygame.init()

# COLORS
BLUE = (0, 0, 255)
GREY = (210, 210, 210)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
PURPLE = (255, 0, 255)
SEA_COLOR = (88, 167, 170)

BACKGROUND_COLOR = SEA_COLOR

# Setting stuff
gameSettings = Settings()
screen = gameSettings.getScreen()
KEYTOBINDING = gameSettings.settings["KEYTOBINDING"]
MOUSEBINDING = gameSettings.settings["MOUSEBINDING"]

# Variables
carryOn = True  # Main game loop
clock = pygame.time.Clock()

x = 0
y = 0
font = pygame.font.Font(None, 30)

mouseData = {"LEFT": False, "RIGHT": False, "MIDDLE": False, x: -100, y: -100, "changed": False}
keyData = {}

for key in KEYTOBINDING.values(): 
    keyData[key] = False

# Drawing
allDraw = drawFunctions(screen)
game = Game(allDraw,screen.get_width())

def draw():
    global gameState, oldGameState
    screen.fill(BACKGROUND_COLOR)
    
    game.draw()

    # Debugging
    fps = font.render(str(int(clock.get_fps())), True, WHITE)
    screen.blit(fps, (50, 50))


def update():
    game.update(mouseData,keyData)
    
# Input
def checkInput():
    global carryOn, turnDEBUG
    #You have to reset each button to false, because otherwise it will get triggerd twice
    for button in MOUSEBINDING:
        mouseData[button] = False
    for key in keyData:
        keyData[key] = False

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            carryOn = False
        elif event.type == pygame.KEYDOWN:
            if event.key == K_F1:
                print("SWITCHING DEBUG MODE")
                game.changeView()
                time.sleep(0.1)
            elif event.key == K_ESCAPE:
                carryOn = False
                time.sleep(0.1)

            # This has to be a string, cuz JSON cant store int's as keys
            if (str(event.key) in KEYTOBINDING):
                keyData[KEYTOBINDING[str(event.key)]] = True
                print("Turning on key: ", KEYTOBINDING[str(event.key)])
        elif event.type == pygame.MOUSEMOTION:
            x, y = event.pos
            mouseData["x"] = x
            mouseData["y"] = y
            mouseData["changed"] = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            x, y = event.pos
            mouseData["x"] = x
            mouseData["y"] = y
            mouseData["changed"] = True
        elif event.type == pygame.MOUSEBUTTONUP:
            x, y = event.pos
            mouseData["x"] = x
            mouseData["y"] = y
            mouseData["changed"] = True
            mouseData[MOUSEBINDING[event.button-1]] = True

#This is to handle CTRL+C!
import signal, sys
def signal_handler(sig, frame):
    print('You pressed Ctrl+C! Exitting')
    gameSettings.save()
    print("SAVED")
    pygame.quit()
    print("QUIT PYGAME")
    exit()   
signal.signal(signal.SIGINT, signal_handler)

# Gameloop
while carryOn:
    checkInput()
    update()
    draw()

    # Refresh Screen
    pygame.display.flip()

    # Number of frames per second
    clock.tick(60) #TODO: Make this standard monitor refresh

print("Exitting game")
gameSettings.save()
pygame.quit()
time.sleep(0.1)
exit()